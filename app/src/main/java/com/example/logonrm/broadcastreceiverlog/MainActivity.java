package com.example.logonrm.broadcastreceiverlog;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private Button btn_enviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_principal);

        btn_enviar = (Button) findViewById(R.id.btn_enviar);
        btn_enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                efeitoApi();
                sendBroadcast(new Intent("PUFFF"));
            }
        });
    }

    public void efeitoApi(){
        ImageView image = (ImageView) findViewById(R.id.imagem);

        PropertyValuesHolder animacao1 = PropertyValuesHolder.ofFloat("alpha", 1f, 0f);
        PropertyValuesHolder animacao2 = PropertyValuesHolder.ofFloat("Y", 120f, 350f);

        ObjectAnimator anim = ObjectAnimator.ofPropertyValuesHolder(image, animacao1, animacao2);
        anim.setDuration(6000);
        anim.start();

    }
}
